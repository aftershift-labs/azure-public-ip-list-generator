# Azure Public IP List Generator

- [Live Demo]( https://az-ips.msenturk.net/ "live demo")

- The pipeline in this project updates and parses the Azure IP address ranges file, which is published weekly, on a daily basis.

&nbsp;

**Usage**

```sh
curl -sL https://az-ips.msenturk.net/ips.json | jq -r .

# or 

curl -sLO https://az-ips.msenturk.net/AzureCloud.txt
```

&nbsp;

**References**

- *Idea:* [Prashanth Kumar's Blog Post](https://prashanth-kumar-ms.medium.com/how-to-add-microsoft-azure-ip-ranges-and-service-tag-based-ip-addresses-873fb346fa3b)
- [Microsoft Download Center / Azure IP Ranges and Service Tags – Public Cloud](https://www.microsoft.com/en-us/Download/confirmation.aspx?id=56519)
- [Azure Service Tags](https://learn.microsoft.com/en-us/azure/virtual-network/service-tags-overview)

&nbsp;
